package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Student;

public class CreateDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {			
			
			/*Instructor tempInstructor = new Instructor("Chad", "Darby" , "darby@luv2code.com");
			
			InstructorDetail tempInstructorDetail = new InstructorDetail("luv2code/youtube", "Luv 2 code!!!");
			*/
			
			Instructor tempInstructor = new Instructor("Anshul", "Patel" , "patel@luv2code.com");
			
			InstructorDetail tempInstructorDetail = new InstructorDetail("luv2code/youtube/starsky", "Cycling");
			
			tempInstructor.setInstructorDetail(tempInstructorDetail);
			
			// start a transaction
			session.beginTransaction();
			
			// save the object
			session.save(tempInstructor);
			System.out.println(tempInstructor);
			
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}
		finally {
			factory.close();
		}
	}

}





