package com.luv2code.spring;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class HelloSpringApp {

	public static void main(String[] args) {
		// load the spring configuration file
		ClassPathXmlApplicationContext  context = new ClassPathXmlApplicationContext("applicationcontext.xml");
		// retrieve bean from spring container
		CricketCoach theCoach = context.getBean("myCrickeCoach",CricketCoach.class);
		// call methods on bean
		System.out.println(theCoach.getDailyWorkout());
		System.out.println(theCoach.getDailyFotune());
		System.out.println(theCoach.getEmailAddress());
		System.out.println(theCoach.getTeam());
		//close the context
		context.close();
	}

}
