package com.luv2code.spring;

public class BaseballCoach implements Coach {
	private FortuneService fortuneService;
	public BaseballCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	
	
	@Override
	public String getDailyWorkout() {
		return "Do Batting Practice for 30 mins";
	}

	@Override
	public String getDailyFotune() {
		return fortuneService.getFortune();
	}
	
	public void startupmethod() {
		System.out.println("startupmethod");
	}
	public void cleanupmethod() {
		System.out.println("cleanupmethod");
	}
	
}
